from enum import Enum
import random
from tkinter import Tk, Canvas
from typing import Literal, Tuple, Union
import neat
import math
import visualize
from neat.nn import FeedForwardNetwork

# Kantenlänge des Labyrinths
MAP_SIZE = 25
THEORETICALLY_SHORTEST_PATH = math.sqrt(MAP_SIZE*MAP_SIZE)
VIEW_RANGE = 1
NUM_INPUTS = (VIEW_RANGE * 2 + 1)**2

class Direction(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

def dijkstra(map: list[list[Union[int, Literal['S', 'E']]]], start: Tuple[int, int], end: Tuple[int, int]) -> int:
    """
        Führt den Dijkstra-Algorithmus auf der Karte aus und gibt die
        kürzeste Route vom Start zum Ende zurück.
    """
    # Quit early if start and end are the same
    if start == end:
        #print("Path: []")
        return 0

    # nodes saves the precursor and distance to the start node
    nodes: list[list[Union[None, Tuple[Union[None, Tuple[int, int]], int]]]] = [[None for _ in range(len(map))] for _ in range(len(map[0]))]
    nodes[start[0]][start[1]] = (None, 0)
    # queue saves the nodes that have to be visited
    queue: list[Tuple[int, int]] = [start]
    while len(queue) > 0:
        current = queue.pop(0)
        # check all neighbors
        for (next_x, next_y) in [(current[0] - 1, current[1]), (current[0] + 1, current[1]), (current[0], current[1] - 1), (current[0], current[1] + 1)]:
            # check if the neighbor is in the map
            if next_x >= 0 and next_x < len(map) and next_y >= 0 and next_y < len(map[0]):
                current_node_data: Tuple[Tuple[int, int], int] = nodes[current[0]][current[1]]  # type: ignore
                next_node_data = nodes[next_x][next_y]
                # check if the neighbor is not a wall and has not been visited yet
                if (map[next_x][next_y] == 0 or map[next_x][next_y] == 'S') and (next_node_data == None or next_node_data[1] > current_node_data[1] + 1):
                    nodes[next_x][next_y] = (current, current_node_data[1] + 1)
                    queue.append((next_x, next_y))
                elif map[next_x][next_y] == 'E':
                    nodes[next_x][next_y] = (current, current_node_data[1] + 1)
                    break

    
    # build the path from the end to the start
    path: list[Tuple[int, int]] = []
    current: Tuple[int, int] = end
    while current != start:
        path.append(current)
        next: Tuple[Tuple[int, int], int] = nodes[current[0]][current[1]] # type: ignore
        current = next[0]
    path.append(start)
    path.reverse()
    
    #print("Path: " + str(path))
    return len(path)

class Agent:
    """
        Repräsentiert einen Agenten, der sich durch das Labyrinth bewegt.
    """
    
    def __init__(self, net: FeedForwardNetwork):
        self.net = net
        self.pos_x = -1
        self.pos_y = -1
        self.goal_x = -1
        self.goal_y = -1
        self.map = []
        self.visited = set()
        self.fitness = 0.0

    def set_map(self, map: list[list[int]]):
        self.map = map

    def set_start(self, x: int, y: int):
        self.pos_x = x
        self.pos_y = y
        self.visited.add((x, y))

    def set_goal(self, x: int, y: int):
        self.goal_x = x
        self.goal_y = y

    def activate_net(self, inputs):
        output = self.net.activate(inputs)
        return output.index(max(output))

    def move(self, direction: Direction):
        """
            Bewegt den Agenten auf dem Labyrinth. Es muss sichergestellt werden,
            dass der Agent nur zulässige Bewegungen macht. Die Methode gibt
            True zurück, wenn die Bewegung erfolgreich war, andernfalls False.
        """
        def is_in_map(x: int, y: int):
            return x >= 0 and x < len(self.map) and y >= 0 and y < len(self.map[0])
        
        def is_wall(x, y):
            return self.map[x][y] == 1
        
        def is_free(x, y):
            return self.map[x][y] == 0 or self.map[x][y] == 'E'
        
        def valid_move(x: int, y: int):
            return is_in_map(x, y) and not is_wall(x, y) and (x,y) not in self.visited and is_free(x, y)
        
        next_x = self.pos_x
        next_y = self.pos_y
        if direction == Direction.UP:
            next_y -= 1
        elif direction == Direction.DOWN:
            next_y += 1
        elif direction == Direction.LEFT:
            next_x -= 1
        elif direction == Direction.RIGHT:
            next_x += 1

        if valid_move(next_x, next_y):
            self.pos_x = next_x
            self.pos_y = next_y
            self.visited.add((next_x, next_y))
            return True

        return False

    def _get_distance(self):
        #return dijkstra(self.map, (self.pos_x, self.pos_y), (self.goal_x, self.goal_y)) # type: ignore
        return math.sqrt((self.goal_x - self.pos_x)**2 + (self.goal_y - self.pos_y)**2)

    def _get_map_env(self):
        env = []
        def get_value(x, y):
            if x < 0 or y < 0 or x >= len(self.map) or y >= len(self.map[0]):
                return 1 # value for out-of-bounds indices
            elif self.map[x][y] == 'E' or self.map[x][y] == 'S':
                return 0
            else:
                return self.map[x][y]
            
        
        x_range = range(self.pos_x - VIEW_RANGE, self.pos_x + VIEW_RANGE+1)
        y_range = range(self.pos_y - VIEW_RANGE, self.pos_y + VIEW_RANGE+1)
        for x in x_range:
            for y in y_range:
                env.append(get_value(x, y))

        return env


    def run(self):
        """
            Führt eine Maximalanzahl von Schritten für den Agenten aus.
            Die Richtung wird vom "Gehirn", dem neuronalen Netz festgelegt.
        """

        step = 0
        while step < 100:
            step += 1
            if self.pos_x == self.goal_x and self.pos_y == self.goal_y:
                break
            inputs = self._get_map_env()
            direction = self.activate_net(inputs)
            if not self.move(Direction(direction)): # type: ignore
                break
        
        self.fitness = -1*(self._get_distance()+step) + 1
        return 

class MapGenerator:
    """
        MapGenerator kümmert sich um die Erzeugung und die Anzeige der
        Labyrinth-Karten.
    """

    def __init__(self, size: int, start: Tuple[int, int], end: Tuple[int, int]):
        self.size = size
        self.start = start
        self.end = end
        self.map: list[list[Union[int,Literal['S', 'E']]]] = [[0 for _ in range(size)] for _ in range(size)]
        self.tilesize = 20
    
    def generate(self):
        """
           Erzeugt eine Zufallskarte und gibt sie als 2D-Array zurück.
           Eine 0 im Array steht für ein betretbares Feld, 1 für ein
           Hindernis, S für Start und E für Ende.
        """
        
        # Sicherstellen, dass es mindestens einen PFad vom Start bis zum Ende gibt
        while True:
            self.map = [[random.randint(0, 1) for _ in range(self.size)] for _ in range(self.size)]
            if self._is_valid():
                break

        # Start und Ende Punkte setzen
        self.map[self.start[0]][self.start[1]] = 'S'
        self.map[self.end[0]][self.end[1]] = 'E'

    def _is_valid(self):
        start, end = self.start, self.end
        queue = [start]
        visited: set[Tuple[int, int]] = set(queue)
        while queue:
            row, col = queue.pop(0)
            if (row, col) == end:
                return True
            neighbors = self._get_neighbors(row, col)
            for neighbor in neighbors:
                if neighbor not in visited and self.map[neighbor[0]][neighbor[1]] == 0:
                    visited.add(neighbor)
                    queue.append(neighbor)
        return False

    def _get_neighbors(self, row: int, col: int):
        neighbors = []
        if row > 0:
            neighbors.append((row-1, col))
        if row < self.size-1:
            neighbors.append((row+1, col))
        if col > 0:
            neighbors.append((row, col-1))
        if col < self.size-1:
            neighbors.append((row, col+1))
        return neighbors

    def draw_map(self, agent: Agent):
        """
            Erstellt und zeigt die GUI an.
        """

        ts = self.tilesize
        root = Tk()
        root.title('NEAT Maze')
        canvas = Canvas(root, width=self.size*ts, height=self.size*ts)
        canvas.pack()
        for row in range(self.size):
            for col in range(self.size):
                if self.map[col][row] == 'S':
                    canvas.create_rectangle(col*ts, row*ts, (col+1)*ts, (row+1)*ts, fill='blue')
                elif self.map[col][row] == 'E':
                    canvas.create_rectangle(col*ts, row*ts, (col+1)*ts, (row+1)*ts, fill='red')
                else:
                    color = 'white' if self.map[col][row] == 0 else 'gray'
                    canvas.create_rectangle(col*ts, row*ts, (col+1)*ts, (row+1)*ts, fill=color)
        # draw the agent's path
        canvas.create_oval(
                    agent.pos_x*ts+ts*0.25, 
                    agent.pos_y*ts+ts*0.25, 
                    (agent.pos_x+1)*ts-ts*0.25, 
                    (agent.pos_y+1)*ts-ts*0.25, 
                    fill='orange')

        for _ in range(0,MAP_SIZE*20):
            inputs = agent._get_map_env()
            output = agent.activate_net(inputs)
            if agent.move(Direction(output)):
                canvas.create_oval(
                    agent.pos_x*ts+ts*0.25, 
                    agent.pos_y*ts+ts*0.25, 
                    (agent.pos_x+1)*ts-ts*0.25, 
                    (agent.pos_y+1)*ts-ts*0.25, 
                    fill='orange')
                
            if agent.pos_x == 0 and agent.pos_y == 0:
                    break

        root.mainloop()



# Creates agents with the given net and tests it on the given map
def eval_genomes(genomes, config):
    """
        Testet jedes Genom mit einem Agenten.
    """
    
    map = config.map
    for genome_id, genome in genomes:
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        agent = Agent(net)
        agent.set_map(map)
        agent.set_start(MAP_SIZE-1, MAP_SIZE-1)
        agent.set_goal(0, 0)
        agent.run()
        genome.fitness = agent.fitness

    return

def main():
    # Erzeugen einer Zufallskarte der Größe 20x20
    generator = MapGenerator(MAP_SIZE, (MAP_SIZE-1, MAP_SIZE-1), (0, 0))
    generator.generate()

    # Laden einer geeigneten NEAT-Konfiguration aus der Datei 'neat-config'
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        'neat-config')
    
    
    config.map = generator.map # type: ignore

    # Erzeugen einer Population
    p = neat.Population(config)

    # Ein Listener, der den Status auf der Konsole loggt
    p.add_reporter(neat.StdOutReporter(False))

    # Statistik-Logger für die Visualisierung des Netzes
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    # Run until a solution is found or generation X is reached.
    winner: neat.DefaultGenome = p.run(eval_genomes, 20) # type: ignore

    #visualize.draw_net(config, winner, True)
    visualize.draw_net(config, winner, True, prune_unused=True)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

    net = neat.nn.FeedForwardNetwork.create(winner, config)
    agent = Agent(net)
    agent.set_map(generator.map) # type: ignore
    agent.set_goal(0,0)
    agent.set_start(MAP_SIZE-1, MAP_SIZE-1)
    return (generator, agent)

generator = None
agent = None
(generator, agent) = main()
    
generator.draw_map(agent)
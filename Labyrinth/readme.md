# NEAT zum Lösen eines Labyrinths

Ein Agent mit einer Sichtweite von 1 soll durch ein neuronales Netz entscheiden, in welche Richtung er weitergeht. Das Netz wird mit NEAT trainiert.

Zur Verdeutlichung weshalb dies nicht funktionieren kann, wurde außerdem ein Agent mit Sichtweite 10 (also mit 441 Feldern im Blickfeld, 441 Input-Neuronen) und 200 hidden Neuronen trainiert. Das Ergebnis ist ein riesiges Netz, durch das der Agent trotzdem immer nur in die Ecke läuft.

![Netz mit 441 Input-Neuronen](./Digraph_Sichtweite10.gv.svg)
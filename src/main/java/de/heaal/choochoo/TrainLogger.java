package de.heaal.choochoo;

public interface TrainLogger{
    void accept(int distanceToTarget, int speed, double consumption);
}
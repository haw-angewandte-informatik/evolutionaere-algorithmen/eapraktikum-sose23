package de.heaal.choochoo;

public class Train {
    private int speed;
    private double totalConsumption;
    private int distanceToTarget;

    public Train(int speed, double totalConsumption, int distanceToTarget) {
        this.speed = speed;
        this.totalConsumption = totalConsumption;
        this.distanceToTarget = distanceToTarget;
    }

    public void accelerate() {
        speed += 1;
    }

    public void decelerate() {
        speed -= 1;
    }

    public void tick(){
        totalConsumption += speed*speed;
        distanceToTarget -= speed;
    }

    public boolean isFinished(){
        return speed == 0 || distanceToTarget < 0;
    }

    public int getSpeed() {
        return speed;
    }

    public double getTotalConsumption() {
        return totalConsumption;
    }

    public int getDistanceToTarget() {
        return distanceToTarget;
    }
}

package de.heaal.choochoo;

import java.util.function.Consumer;

import io.jenetics.prog.ProgramChromosome;

public class Track {
    public static final int DISTANCE = 100;

    private static double distanceWeight = 100; //LOSS MAX -> 10.000
    private static double timeWeight = 20; // LOSS MAX -> 2.000
    private static double consumptionWeight = 0.1; //LOSS MAX -> ca. 70

    public static double go(ProgramChromosome<Double> program) {
        return go(program, null, null);
    }

    /**
     * Runs the train with the given program and returns the loss
     * @param program The program to run
     * @param debugLogger A consumer that will be called with debug messages
     * @return trainLogger Will be called with current distance, speed and consumption
     */
    public static double go(ProgramChromosome<Double> program, Consumer<String> debugLogger, TrainLogger trainLogger) {
        Train train = new Train(1, 0, DISTANCE);
        int ticks = 0;
        while (!train.isFinished()) {
            // Positive answer means accelerate, negative means decelerate
            var answer = program.eval(((double) train.getDistanceToTarget()), (double)train.getSpeed());
            if (answer > 0) {
                train.accelerate();
            } else if (answer < 0) {
                train.decelerate();
            }
            train.tick();
            ticks++;
            if (debugLogger != null)
                debugLogger.accept(ticks + ") Distance: " + train.getDistanceToTarget() + " Speed: " + train.getSpeed() + " Consumption: " + train.getTotalConsumption());
            if (trainLogger != null)
            trainLogger.accept(train.getDistanceToTarget(), train.getSpeed(), train.getTotalConsumption());
        }

        return calculateLoss(train, ticks);
    }

    public static double calculateLoss(Train train, int ticks) { // low == good
        var distLoss = train.getDistanceToTarget() < 0 ? (Math.abs(train.getDistanceToTarget())*distanceWeight*10) : train.getDistanceToTarget()*distanceWeight;
        return train.getTotalConsumption() * consumptionWeight +
                ticks * timeWeight +
                distLoss;
    }
}

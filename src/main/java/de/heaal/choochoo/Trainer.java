package de.heaal.choochoo;

import java.util.function.Consumer;

import io.jenetics.Genotype;
import io.jenetics.Mutator;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.engine.Limits;
import io.jenetics.ext.SingleNodeCrossover;
import io.jenetics.prog.ProgramChromosome;
import io.jenetics.prog.ProgramGene;
import io.jenetics.prog.op.EphemeralConst;
import io.jenetics.prog.op.MathExpr;
import io.jenetics.prog.op.MathOp;
import io.jenetics.prog.op.Op;
import io.jenetics.prog.op.Var;
import io.jenetics.util.Factory;
import io.jenetics.util.ISeq;

public class Trainer {
    public static void main(String[] args) {
        ISeq<Op<Double>> ops = ISeq.of(
                MathOp.ADD,
                MathOp.SUB,
                MathOp.MUL,
                MathOp.DIV,
                MathOp.POW,
                MathOp.SQRT,
                MathOp.EXP,
                MathOp.LOG,
                MathOp.MAX,
                MathOp.MIN);

        ISeq<Op<Double>> terminals = ISeq.of(
                Var.of("distanceToTarget"),
                Var.of("speed"),
                EphemeralConst.of(() -> Math.random() * 100));

        Factory<Genotype<ProgramGene<Double>>> gtf = () -> Genotype.of(ProgramChromosome.of(2, ops, terminals));

        var engine = Engine
                .builder(
                        genotype -> Track.go((ProgramChromosome<Double>)genotype.chromosome()),
                        gtf
                        )
                .minimizing()
                .populationSize(1000)
                .alterers(new SingleNodeCrossover<>(0.1), new Mutator<>())
                .build();

        var result = engine.stream()
                .limit(Limits.byFixedGeneration(1000))
                .limit(Limits.byFitnessThreshold(100.0))
                .collect(EvolutionResult.toBestEvolutionResult());

        var finalProgram = result.bestPhenotype().genotype().chromosome();
        Consumer<String> debugLogger = msg -> System.out.println(msg);
        var bestResult = Track.go((ProgramChromosome<Double>)finalProgram, null, Trainer::printTrainPosition);
        System.out.println("Best result: " + bestResult);
        System.out.println("Genome: " + finalProgram);
        var gene = finalProgram.gene();
        var tree = gene.toTreeNode();
        System.out.println("Program: " + tree);
        System.out.println(MathExpr.format(finalProgram.gene().toTreeNode()));
    } 

    private static int lastSpeed = 1;
    // Fancy train when our terminal learns to unicode: [%dkm].[%dkm/h].[%dl].[⊞]⫤>
    private static String formatTrainPosition(int distance, int speed, double consumption) {
        var sb = new StringBuilder();
        int i = 0;
        sb.append("💨");
        for (; i <= Track.DISTANCE-distance; i++) {
            sb.append("_");
        }
        // make powershell green if speed faster than previous speed, or red when braking
        if (speed > lastSpeed) {
            sb.append("\u001B[32m");
        } else if (speed < lastSpeed) {
            sb.append("\u001B[31m");
        }
        sb.append(String.format("[[%2dkm].[%2dkm/h].[%3dl]]=>", distance, speed, (int)consumption));
        lastSpeed = speed;
        sb.append("\u001B[0m");
        for (; i <= Track.DISTANCE; i++) {
            sb.append("_");
        }
        sb.append("🏁");
        return sb.toString();
    }

    private static void printTrainPosition(int distance, int speed, double consumption) {
        System.out.println(formatTrainPosition(distance, speed, consumption));
        // Timeout 1s
        try {
            Thread.sleep(750);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

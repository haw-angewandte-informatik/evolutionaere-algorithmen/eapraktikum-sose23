package de.heaal.eaf.crossover;

import java.util.Random;

import de.heaal.eaf.base.Individual;

public class AverageCombiner<T extends Individual> implements Combination<T> {

    @Override
    public void setRandom(Random rng) {
        return;
    }

    @Override
    public T combine(Individual[] parents) {
        T child = (T) parents[0].copy();
        for(int i = 0; i < child.getGenome().len(); i++){
            float sum = 0;
            for(Individual parent : parents){
                sum += parent.getGenome().array()[i];
            }
            child.getGenome().array()[i] = (sum / parents.length);
        }

        return child;
    }
}

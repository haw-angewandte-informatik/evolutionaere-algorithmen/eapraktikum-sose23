package de.heaal.eaf.testbench;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class AccelerometerData {
    public final int Timestamp;
    //public final float X;
    //public final float Y;
    //public final float Z;
    public final float Abs;

    public static final DecimalFormat format = new DecimalFormat("#,###", DecimalFormatSymbols.getInstance(java.util.Locale.GERMAN));

    public static AccelerometerData FromStrings(String[] data) {
        try {
            return new AccelerometerData(
                Integer.parseInt(data[0]),
                //format.parse(data[1]).floatValue(),
                //Float.parseFloat(data[2]),
                //Float.parseFloat(data[3]),
                format.parse(data[4]).floatValue()
            );
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    public AccelerometerData(
        int timestamp, 
        //float x, 
        //float y, 
        //float z, 
        float abs) {
        Timestamp = timestamp;
        //X = x;
        //Y = y;
        //Z = z;
        Abs = abs;
    }
}//

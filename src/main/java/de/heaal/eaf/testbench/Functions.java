package de.heaal.eaf.testbench;

import java.util.function.Function;

import de.heaal.eaf.base.Individual;

public abstract class Functions {
    public static Function<Individual, Float> evalSphereFunc2D =
    (ind) -> {
        var x0 = ind.getGenome().array()[0];
        var x1 = ind.getGenome().array()[1];
        return x0 * x0 + x1 * x1;
    };

    public static Function<Individual,Float> ackleyTestFunction =
    (ind) -> {
        var x0 = ind.getGenome().array()[0];
        var x1 = ind.getGenome().array()[1];
        return ackley(x0, x1);
    };

    public static Function<Individual, Float> sinus = 
    (ind) -> {
        var A = ind.getGenome().array()[0];
        var f = ind.getGenome().array()[1];
        var phi = ind.getGenome().array()[2];
        var D = ind.getGenome().array()[3];

        var data = Data.getData();
        var result = 0f;
         for(var value : data){
            result += Math.pow(sinus(A, f, phi, D, value.Timestamp) - value.Abs, 2);
        }
        return result;
    };

    private static float ackley(float x, float y){

        return (float) ((float) - 20 * Math.exp(-0.2 * Math.sqrt(0.5 *(x*x + y*y)))
        - Math.exp(0.5 * (Math.cos(2*Math.PI*x) * Math.cos(2*Math.PI*y)))
                + 20 + Math.E);

    }

    private static float sinus(float A, float f, float phi, float D, float t){
        return (float) (A * Math.sin(2 * Math.PI * f * t + phi) + D);
    }
}

package de.heaal.eaf.testbench;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReaderBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Data {
    private static ArrayList<AccelerometerData> dataCache;

    public static ArrayList<AccelerometerData> getData() {
        if (dataCache == null) {
            try (var reader = new CSVReaderBuilder(new FileReader("sensordata.csv"))
            .withCSVParser(new CSVParserBuilder().withSeparator(';').build())
            .withSkipLines(1)
            .build()) {
                var result = new ArrayList<AccelerometerData>();
                reader.forEach((data) -> result.add(AccelerometerData.FromStrings(data)));
                dataCache = result;
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return dataCache;
    }
}

package de.heaal.eaf.testbench;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import javax.swing.text.html.HTML.Tag;

import com.opencsv.CSVWriter;

import de.heaal.eaf.algorithm.DifferentialEvolutionAlgorithm;
import de.heaal.eaf.algorithm.GeneticAlgorithm;
import de.heaal.eaf.algorithm.HillClimbingAlgorithm;
import de.heaal.eaf.base.Algorithm;
import de.heaal.eaf.base.GenericIndividualFactory;
import de.heaal.eaf.base.Individual;
import de.heaal.eaf.crossover.AverageCombiner;
import de.heaal.eaf.crossover.SinglePointCrossover;
import de.heaal.eaf.evaluation.ComparatorIndividual;
import de.heaal.eaf.evaluation.MinimizeFunctionComparator;
import de.heaal.eaf.mutation.RandomMutation;

public class AlgorithmComparison {
    private final HashMap<String, Algorithm<Individual>> algos;
    private final CSVWriter fitnessWriter;
    private final CSVWriter resultWriter;
    private Function<Individual, Float> fitnessFunction;

    public final static int MAX_ITERATIONS = 2000;

    public AlgorithmComparison(Function<Individual, Float> fitnessFunction) throws IOException {
        this.fitnessFunction = fitnessFunction;
        algos = new HashMap<>();
        fitnessWriter = new CSVWriter(new FileWriter("test.csv"));
        resultWriter = new CSVWriter(new FileWriter("bestGenomes.csv"));
    }

    public static void main(String[] args) throws IOException {
        float[] minAckley = { -5.12f, -5.12f };
        float[] maxAckley = { +5.12f, +5.12f };
        float[] minSinus = { 0f, 0.1f, (float) 0, 0f };
        float[] maxSinus = { 100f, 100f, (float) ((float) 2*Math.PI), 20f };

        Comparator<Individual> compAckley = new MinimizeFunctionComparator<>(Functions.ackleyTestFunction);
        Comparator<Individual> compSinus = new MinimizeFunctionComparator<>(Functions.sinus);

        var comparer = new AlgorithmComparison(Functions.ackleyTestFunction);
        var terminationCriterion = new ComparatorIndividual(0.1f);
        var crossOver = new SinglePointCrossover<>();
        crossOver.setRandom(new Random(0));
        var averager = new AverageCombiner<>();
        var mutator = new RandomMutation(minSinus, maxSinus);

        var gaWithoutElitism = new GeneticAlgorithm(100, minSinus, maxSinus, compSinus,
                mutator, terminationCriterion,
                crossOver, 0);
        var gaWithElitism = new GeneticAlgorithm(100, minSinus, maxSinus, compSinus,
                mutator, terminationCriterion,
                crossOver, 1);
        var gaWithoutElitismAvg = new GeneticAlgorithm(100, minAckley, maxAckley, compAckley,
                new RandomMutation(minAckley, maxAckley), terminationCriterion,
                averager, 0);
        var gaWithElitismAvg = new GeneticAlgorithm(100, minAckley, maxAckley, compAckley,
                new RandomMutation(minAckley, maxAckley), terminationCriterion,
                averager, 1);
        var hc = new HillClimbingAlgorithm(minAckley, maxAckley, compAckley, new RandomMutation(minAckley, maxAckley),
                terminationCriterion);
        var de = new DifferentialEvolutionAlgorithm(compSinus, null, new Random(),
                new GenericIndividualFactory(minSinus, maxSinus), terminationCriterion, 200, 0.2f, 0.4f);

        // comparer.addAlgorithm("gaWithoutElitism", gaWithoutElitism);
        comparer.addAlgorithm("gaWithElitism", gaWithElitism);
        // comparer.addAlgorithm("gaWithoutElitismAvg", gaWithoutElitismAvg);
        // comparer.addAlgorithm("gaWithElitismAvg", gaWithElitismAvg);
        // comparer.addAlgorithm("hc", hc);
        comparer.addAlgorithm("de", de);

        comparer.run();

    }

    public void addAlgorithm(String tag, Algorithm<Individual> algo) throws IOException {
        algos.put(tag, algo);
        // writers.put(tag, new CSVWriter(new FileWriter(tag+".csv")));
    }

    public void run() throws IOException {
        for (var algo : algos.entrySet()) {
            AtomicInteger generation = new AtomicInteger(0);
            algo.getValue().observe(population -> {
                logGeneration(algo.getKey(), generation.get(), population);
                generation.set(generation.get() + 1);
            });
            algo.getValue().run();
            logGeneration(algo.getKey(), generation.get(), algo.getValue().getIndividuals());
            logBestIndividual(algo.getKey(), algo.getValue().getIndividuals());
        }
        fitnessWriter.close();
        resultWriter.close();
    }

    private void logGeneration(String algoName, int generation, List<Individual> population) {
        // log best individual
        // System.out.printf("%s: %f %n", algoName,
        // this.fitnessFunction.apply(population.get(0)));
        String[] data = {
                algoName,
                Integer.toString(generation),
                Float.toString(this.fitnessFunction.apply(population.get(0)))
        };
        fitnessWriter.writeNext(data);
    }

    private void logBestIndividual(String algoName, List<Individual> population) {
        var bestIndividual = population.stream().min(Comparator.comparing(this.fitnessFunction)).get();
        var data = new ArrayList<String>();
        data.add(algoName);
        for(var gene : bestIndividual.getGenome().array()){
            data.add(String.valueOf(gene));
        }
        var error = this.fitnessFunction.apply(bestIndividual);
        data.add(String.valueOf(error));
        resultWriter.writeNext(data.toArray(new String[0]));
    }
}

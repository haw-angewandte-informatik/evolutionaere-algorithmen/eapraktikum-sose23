package de.heaal.eaf.algorithm;

import java.util.Comparator;
import java.util.Random;

import de.heaal.eaf.base.AbstractIndividual;
import de.heaal.eaf.base.Algorithm;
import de.heaal.eaf.base.Individual;
import de.heaal.eaf.base.IndividualFactory;
import de.heaal.eaf.base.Population;
import de.heaal.eaf.base.VecN;
import de.heaal.eaf.evaluation.ComparatorIndividual;
import de.heaal.eaf.mutation.Mutation;
import de.heaal.eaf.selection.SelectionUtils;
import de.heaal.eaf.testbench.AlgorithmComparison;
import de.heaal.eaf.testbench.Functions;

public class DifferentialEvolutionAlgorithm extends Algorithm<Individual> {
    private final IndividualFactory<AbstractIndividual> indFac;
    private final ComparatorIndividual terminationCriterion;
    private final int populationCounter;
    private final float stepsize;
    private final float crossoverRate;

    public DifferentialEvolutionAlgorithm(Comparator<Individual> comparator, Mutation mutator, Random rng,
            IndividualFactory<AbstractIndividual> indFac, ComparatorIndividual terminationCriterion,
            int populationCounter,
            float stepsize,
            float crossoverRate) {
        super(comparator, mutator, rng);
        this.indFac = indFac;
        this.terminationCriterion = terminationCriterion;
        this.populationCounter = populationCounter;
        this.stepsize = stepsize;
        this.crossoverRate = crossoverRate;
    }

    @Override
    protected boolean isTerminationCondition() {
        return comparator.compare(population.get(0), terminationCriterion) > 0;
    }

    @Override
    public void run() {
        initialize(indFac, populationCounter);

        int iterationCounter = 0;
        while (!isTerminationCondition() && iterationCounter < AlgorithmComparison.MAX_ITERATIONS) {
            iterationCounter++;
            super.nextGeneration();
            var children = new Individual[population.size()];
            for (int i = 0; i < population.size(); i++) {
                Individual[] parents = new Individual[3];
                int randomMutationIndex = rng.nextInt(population.size()); // J
                parents[0] = SelectionUtils.selectNormal(population, rng, null);
                parents[1] = SelectionUtils.selectNormal(population, rng, parents[0]);
                parents[2] = SelectionUtils.selectUniform(population, rng, parents);

                Individual child = population.get(i).copy();
                VecN mutatedVector = parents[0].getGenome().copy()
                        .add(
                                parents[1].getGenome().copy().sub(
                                    parents[2].getGenome()
                                ).mul(stepsize)
                            );
                for (int j = 0; j < child.getGenome().len(); j++) {
                    if (rng.nextDouble() < crossoverRate || j == randomMutationIndex) {
                        child.getGenome().array()[j] = mutatedVector.array()[j];
                    }
                }
                children[i] = child;
            }
            for (int i = 0; i < children.length; i++) {
                if (comparator.compare(children[i], population.get(i)) > 0) {
                    population.set(i, children[i]);
                }
            }
        }
    }

}

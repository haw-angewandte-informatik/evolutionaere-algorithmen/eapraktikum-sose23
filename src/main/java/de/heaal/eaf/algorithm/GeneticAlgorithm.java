package de.heaal.eaf.algorithm;

import de.heaal.eaf.base.*;
import de.heaal.eaf.crossover.Combination;
import de.heaal.eaf.evaluation.ComparatorIndividual;
import de.heaal.eaf.mutation.Mutation;
import de.heaal.eaf.mutation.MutationOptions;
import de.heaal.eaf.selection.SelectionUtils;
import de.heaal.eaf.testbench.AlgorithmComparison;

import java.util.*;

public class GeneticAlgorithm extends Algorithm<Individual> {
    private final IndividualFactory<AbstractIndividual> indFac;
    private final ComparatorIndividual terminationCriterion;
    private final int populationCounter;
    private final Combination<Individual> combiner;
    private final int elitismLevel;

    public GeneticAlgorithm(int populationCounter, float[] min, float[] max,
            Comparator<Individual> comparator, Mutation mutator,
            ComparatorIndividual terminationCriterion,
            Combination<Individual> combiner,
            int elitismLevel) {
        super(comparator, mutator);
        this.indFac = new GenericIndividualFactory(min, max);
        this.terminationCriterion = terminationCriterion;
        this.populationCounter = populationCounter;
        this.combiner = combiner;
        this.elitismLevel = elitismLevel;
    }

    @Override
    protected boolean isTerminationCondition() {
        return comparator.compare(population.get(0), terminationCriterion) > 0;
    }

    @Override
    public void run() {
        initialize(indFac, populationCounter);
        int iterationCounter = 0;
        // sort population
        population.sort(comparator);
        do {
            iterationCounter++;
            super.nextGeneration();

            // choose parents and create children
            Population<Individual> childrenList = new Population<Individual>(population.size());
            while (childrenList.size() < population.size() - elitismLevel) {
                Individual[] parentTupel = new Individual[2];
                parentTupel[0] = SelectionUtils.selectNormal(population, rng, null);
                parentTupel[1] = SelectionUtils.selectNormal(population, rng, parentTupel[0]);

                Individual childOne = combiner.combine(parentTupel);
                Individual childTwo = combiner.combine(parentTupel);
                childrenList.add(childOne);
                childrenList.add(childTwo);
            }

            int mutationCounter = (int) (Math.random() * population.size());
            MutationOptions mutationOptions = new MutationOptions();
            mutationOptions.put(MutationOptions.KEYS.MUTATION_PROBABILITY, 0.5f);

            for (int i = 0; i < mutationCounter; i++) {
                Individual child = childrenList.get(i);
                mutationOptions.put(MutationOptions.KEYS.FEATURE_INDEX,
                        (int) (Math.random() * child.getGenome().len()));
                mutator.mutate(child, mutationOptions);
            }

            for (int i = 0; i < elitismLevel; i++)
                childrenList.add(population.get(i));

            // replace old population with new children
            childrenList.sort(comparator);

            population = childrenList;
        } while (!isTerminationCondition() && iterationCounter < AlgorithmComparison.MAX_ITERATIONS);
    }
}
